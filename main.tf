variable "awsprops" {
    default = {
        region = "us-east-1"
        vpc = "vpc-0411eab5b8a65fece" 
        ami = "ami-041feb57c611358bd"
        itype = "t2.micro"
        subnet = "subnet-0e9b11a39777545b7"
        publicip = true
        keyname = "aws-login-key"
        secgroupname = "tf-my-ec2-sec-grp"
  }
 }

provider "aws" {
    region = lookup(var.awsprops,"region")
}

resource "aws_security_group" "project-iac-sg" {
   name = lookup(var.awsprops, "secgroupname")
   description = lookup(var.awsprops, "secgroupname")
   vpc_id = lookup(var.awsprops, "vpc")

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
 }


resource "aws_instance" "project-iac" {
   ami = lookup(var.awsprops, "ami")
   instance_type = lookup(var.awsprops, "itype")
   subnet_id = lookup(var.awsprops, "subnet") #FFXsubnet2
   associate_public_ip_address = lookup(var.awsprops, "publicip")
   key_name = lookup(var.awsprops, "keyname")


  vpc_security_group_ids = [
    aws_security_group.project-iac-sg.id
  ]
#   root_block_device {
#     delete_on_termination = true
#     iops = 150
#     volume_size = 50
#     volume_type = "gp2"
#   }
  tags = {
    Name ="tf-my-ec2-instance"
    Environment = "DEV"
    OS = "UBUNTU"
    Managed = "IAC"
  }
     depends_on = [ aws_security_group.project-iac-sg ]

     user_data = <<-EOF
        #!/bin/bash
        echo "Hello from user data script!" > /tmp/user_data_output.txt

        # Update the system
        sudo yum update -y

        # Install Docker dependencies
        sudo yum install -y docker

        # Start the Docker service
        sudo service docker start

        # Add the user to the docker group (optional, allows running Docker commands without sudo)
        sudo usermod -aG docker ec2-user

        # Enable Docker to start on boot
        sudo chkconfig docker on

        sudo yum install nginx -y

        sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak

        sudo sed -i '/^#/d' /etc/nginx/nginx.conf
        sudo sed -i '/error_page 404/i\
        \
        location /flask {\
            proxy_pass http://localhost:8085/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /appointments {\
            proxy_pass http://localhost:8081/;\
        }\
        \
        ' /etc/nginx/nginx.conf

                sudo sed -i '/error_page 404/i\
        \
        location /doctor {\
            proxy_pass http://localhost:8082/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /patient {\
            proxy_pass http://localhost:8083/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        sudo sed -i '/error_page 404/i\
        \
        location /frontend {\
            proxy_pass http://localhost:8084/;\
        }\
        \
        ' /etc/nginx/nginx.conf

        
        sudo systemctl start nginx
        sudo systemctl enable nginx

        EOF


 }


output "ec2instance" {
  value = aws_instance.project-iac.public_ip
}

